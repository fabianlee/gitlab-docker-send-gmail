FROM python:3.11-slim-bullseye

# Extra python env
ENV DEBIAN_FRONTEND=noninteractive
ENV LANG=en_US.UTF-8
ENV PYTHONDONTWRITEBYTECODE=0
ENV PYTHONUNBUFFERED=1
ENV PIP_DISABLE_PIP_VERSION_CHECK=1

# pip modules determined from 'pip freeze' during local development
COPY requirements.txt /.
RUN set -ex \
  && pip install -r requirements.txt

# copy source and make executable
COPY *.py /.
RUN chmod ugo+r+x *.py

# run as non-root for security
RUN groupadd -g 2000 mygroup 
RUN useradd -G mygroup -u 2001 myuser
USER myuser

# if debugging
#ENTRYPOINT [ "/bin/bash" ]
CMD [ "python", "./send_html_email.py" ]
